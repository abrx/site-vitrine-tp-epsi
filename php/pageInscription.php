<!DOCTYPE html>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="Content-Language" content="fr" />
    <meta name="Description" content="" />
    <meta name="Keywords" content="Tutoriel Bootstrap avec une page d'inscriptionl" />
    <meta name="Subject" content="" />
    <meta name="Content-Type" content="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../css/style.css" />
    <script type="text/javascript" src="../js/traiterInscription.js"></script>
    <title>EcoInfo.fr</title>
  </head>

<body class="my_background">

  <div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
    <div class="row">
      <div class="col-md-offset-2 col-md-8">
        <h1> Inscription <br/> <small> Merci de renseigner vos informations </small></h1>
      </div>
    </div>
  </div>

    <div class="panel-body" >
    <div class="row">
      <div class="col-md-offset-2 col-md-3">
        <form   name = "inscription "method = "post" action = "pageInscription.php" onsubmit="return verifForm(this)">
        <div class="form-group">
          <label for="Nom">Pseudo</label>
          <input type="text" class="form-control" id="username" placeholder="username" onblur="verifPseudo(this)" >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-offset-2 col-md-7">
        <div class="form-group">
          <label for="Email">E-mail</label>
          <input type="text" class="form-control" id="email" placeholder="Entrer une Adresse email" name = "mail" onblur="verifMail(this)" >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-offset-2 col-md-3">
        <div class="form-group">
          <label for="Password">Mot de passe</label>
          <input type="password" class="form-control" id="password" placeholder="Mot de passe" name = "mdp" >
        </div>
      </div>
      <div class="col-md-offset-1 col-md-3">
        <div class="form-group">
          <label for="Vpassword">Vérification mot de passe</label>
          <input type="password" class="form-control" id="vpassword" placeholder="Vérification mot de passe">
          <div id = "rpMdp">
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-offset-2 col-md-3">
        <div class="input-group">
          <span class="input-group-addon glyphicon glyphicon-earphone"></span>
          <input type="text" class="form-control" placeholder="Téléphone" aria-describedby="basic-addon1">
        </div>
        <div class="input-group">
          <span class="input-group-addon glyphicon glyphicon-globe"></span>
          <input type="text" class="form-control" placeholder="Adresse" aria-describedby="basic-addon1">
        </div>
      </div>
    </div>

    <br/>
    <div class="row">
      <div class="col-md-offset-5 col-md-1">
        <button type="submit" name = 'envoyer' class="btn btn-primary" >Envoyer mes informations </button>
      </div>
    </div>
  </form>
  </div>
  </div>
  </div>
  </body>
</html>

<?php
/* Connexion à une base ODBC avec l'invocation de pilote */
$dsn = 'mysql:dbname=ecoinfo;host=127.0.0.1';
$user = 'root';
$password = '';

try {
    $dbh = new PDO($dsn, $user, $password);
    echo 'connecté';
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}

if (isset($_POST['envoyer'])){
                if(empty($_POST['username']) || empty($_POST['mdp']) || empty($_POST['mail']))
                {
                  echo "requete pas executé";
                }
                else{
                  $username=$_POST['username'];
                  $password=$_POST['mdp'];
                  $email=$_POST['mail'];
                  $dbh->exec("INSERT INTO utilisateur(idUtilisateur,username,password,email,etat) VALUES('','$username','$password','$email','0')");
                  echo "requete executé";
                }
            }else{
              echo "requete pas executé";
            }
?>
