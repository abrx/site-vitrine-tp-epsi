<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="Content-Language" content="fr" />
    <meta name="Description" content="" />
    <meta name="Keywords" content="Tutoriel Bootstrap avec une page d'inscriptionl" />
    <meta name="Subject" content="" />
    <meta name="Content-Type" content="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/style.css" />
  <title>EcoInfo.fr</title>
</head>

<body class="my_background">

  <div class="container" >
    <div class="panel panel-default">
    <div class="panel-heading">
    <div class="row">
      <div class="col-md-offset-5 col-md-8" >
        <h1> Connexion <br/></h1>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="row">
      <form   name = "connexion" method = "post" action = "pageConnexion.php" >
      <div class="col-md-offset-4 col-md-3" >
        <div class="form-group">
          <label for="Nom">Pseudo</label>
          <input type="text" class="form-control" id="username" placeholder="username" name = "username">
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-offset-4 col-md-3">
        <div class="form-group">
          <label for="Password">Mot de passe</label>
          <input type="password" class="form-control" id="password" placeholder="Mot de passe" name = "pass">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-offset-5 col-md-1">
        <button type = "submit" name = "connexion" class="btn btn-primary">Connexion</button>
      </div>
    </div>
    </form>
  </div>

  </body>
</html>


<?php
$dsn = 'mysql:dbname=ecoinfo;host=127.0.0.1';
$user = 'root';
$password = '';

try {
    $dbh = new PDO($dsn, $user, $password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo 'connecté';
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}


// pensez a ouvrir une connexion vers mysql ici

if(isset($_POST['connexion'])) {
  if(empty($_POST['username']) || empty($_POST['pass']))
  {
    echo "Veuillez remplir les champs";
  }else{
    echo $_POST['username'];
    echo $_POST['pass'];
    $req = $dbh->prepare('SELECT idUtilisateur FROM utilisateur WHERE username = :username AND password = :password');
    //ATTENTION ton mot de passe n'est pas hashé => en clair
    $req -> bindValue('username',$_POST['username'],PDO::PARAM_STR);
    $req -> bindValue('password',$_POST['pass'],PDO::PARAM_STR);
    $req->execute(array(
     'username'=> $_POST['username'],
     'password'=> $_POST['pass']));

     $resultat = $req->fetch();

      if(empty($resultat['idUtilisateur'])) {
        echo '<p>Mauvais login / password. Merci de recommencer</p>';

      }
      else {
        session_start();
        $_SESSION['login'] = $resultat['partenaires'];
        echo 'Vous etes bien logué';
        header("Location: intro.html");

        // ici vous pouvez afficher un lien pour renvoyer
        // vers la page d'accueil de votre espace membres
    }

  }

}

?>
